@extends ('adminlte.master')

@section('content')
<div class="mt-2 ml-2">
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">Edit Pertanyaan {{$pertanyaan->id}}</h3>
              </div>
              <div class="md-1 mt-2 ml-3">
              <a href="/pertanyaan" class="btn btn-primary btn-sm">Undo</a>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
              	@csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="judul">Judul</label>
                    <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul',$pertanyaan->judul)}}" placeholder="Masukkan Judul">
					@error('judul')
					    <div class="alert alert-danger">{{ $message }}</div>
					@enderror
                  </div>
                  <div class="form-group">
                    <label for="isi">Isi</label>
                    <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $pertanyaan->isi)}}" placeholder="Masukkan Isi">
                    @error('isi')
					    <div class="alert alert-danger">{{ $message }}</div>
					@enderror
                  </div>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
</div>
@endsection